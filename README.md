# 2180607518
|       Title    		|Customer check out cart and makes payment            
|------------------------|-------------------------------|
| Value | As a customer, I want to be check out my cart and payment, so I can review the products I choose.|
|Acceptance Criteria	|1. Scenario 1: <br> Given that there are products in the shopping cart. When the customer requests to view the shopping cart. Then ensure the product has been added to the shopping cart, allowing customer to add or delete and proceed to checkout.<br> 2. Scenario 2: <br> Given that there are no products in the shopping cart. When the customer requests to view the shopping cart. Then ensure to notify the customer that the prodict is not available and suggest the customer to return to the purchase page.             |
|Definition of Done         |<ul> <li>Unit Tests Passed</li> <li>Acceptance Criteria Met</li> <li>Code Reviewed</li> <li>Functional Tests Passed</li> <li>Non-Functional Requirements Met</li> <li>Product Owner Accepts User Story</li> </ul>|
|Owner          |Responsible person: Huynh Truong Hoan|
|Interation | Unscheduled|
|Estimate: | 5 Points |

![](cart.png)
![](cart1.png)

# 2180608573
| Title: | Manage Books |
| ------ | ------ |
| Value Statement: | As a book manager, I want to update, delete and search books in the database, so that I can manage the inventory and availability of books. |
| Acceptance Criteria: | <ul> <li>The book manager can access a web page with a form to enter the book details (title, author, genre, ISBN, etc.)</li> <li>The book manager can submit the form and see a confirmation message that the book has been added to the database</li> <li>The book manager can view a list of all the books in the database with their details</li> <li>The book manager can edit or delete any book from the list by clicking on a button</li> <li>The book manager can search for a book by entering a keyword or a filter (e.g. genre, author, etc.)</li> </ul> |
| Definition of Done | <ul> <li>Acceptance Criteria Met</li> <li>Code reviewed</li> <li>Product owner Accepts User story</li> </ul> |
| Owner | Book Manager |
| Iteration | Unscheduled |
| Estimate | 5 Points |
 
# Main User Interface
![mainUS](/uploads/ef83a568d4fe0e88b63ac7d80b71e02e/mainUS.png)




# Menu Items – Specific Books
![menuUS](/uploads/ac5c8efeaece0f83d9378f96bcfa5aeb/menuUS.png)
![comboBoxUS](/uploads/a386299f7d106b147dbeb31e8aa6c87a/comboBoxUS.png)

# Form Edit Book
![editUS](/uploads/ca66ffcbae22df17b4c90874564bdabd/editUS.png)

# Form Book Detail
![detailUS](/uploads/b346206f14e26491ea2e2215aab14939/detailUS.png)

# Form Import Books
![importUS](/uploads/841aec3b54ef1b3274810f7c21ec07a5/importUS.png)


|      Title          |   Show menu of books                                         ||
|----------------|-------------------------------|-----------------------------|
|Value Statement|        As a bookseller, I want to be able to view a menu that lists all the available books to the customer, so that they can easily browse and select the book they want to buy. |          |
|Acceptance Criteria          |Given the menu display a list of books, each book should be displayed with its title, author, and a brief description.<br>When the customers open app or click on the list of books. <br> Then a list with many book genres appears, this menu should be easy to navigate and scroll through and Clicking on a book should open a detailed view or page providing more information and option as adding it to wishlist or purchasing it              |      |     
|Definition of Done         |Unit Tests Passed <br> Acceptance Criteria Met <br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story||
|		Owner			|			Tran Thanh Tuong					|			|				
|			Iteration		|		Unscheduled							|			|			
|					Estimate|5 Points								||

![giaodien](https://gitlab.com/2180608203/2180608203/uploads/ed29aa7b315d76d44ba8ca3fef908627/giaodien.png)


| Title: | FeedBack |
| :-------- | :----------------- |
| Value Statement: | As a customer, <br>I would like to be able to give feedback on the product, <br> my experience or submit store advice, to share your opinion and help improve the store's services.  |
| Acceptance Criteria: | Acceptance Criterion 1:  <br>Customers need to be notified of successful submission of their feedback or advice <br>All feedback must be stored in the store's database for future reference and analysis <br>If necessary, the store should have the ability to contact customers to request additional information or provide personal feedback <br> <br> <br>Acceptance Criterion 2: <br>If any changes or improvements are made to products or services based on customer feedback, the store should notify customers about this. <br>The store should have a clear policy on accepting or rejecting inappropriate or rule-violating feedback <br>The system needs to undergo performance testing to ensure that the process of providing feedback does not negatively impact the user experience <br> |
| Definition of Done: |  - Unit test passed <br> - Acceptance criteria are met <br> - The code has been evaluated <br> - Functional test passed <br> - Non-functional requirements have been met <br> - Save and rate tests |
| Owner: | Khang | Owner |
| Iteration: | Unscheduled |
| Estimate: | 5 Points |


# 2180608413


![](Figma_basics.png)



|       Title    		| Search books |
|------------------------|-------------------------------|
|Value Statement	|As a customer, I want to find books quickly so I can view or buy the books I want            |
|Acceptance Criteria        |Given that  to already know the information of book .<br>When customers enter into the search box.<br> Then a list of related books appears for customers to search.|
| Definition of Done| 1. performs validation and error handling for inputs and outputs.<br>2.meets the performance, security, usability, and reliability requirements.<br>3. passes all the unit tests, integration tests, and acceptance tests.
|Owner          |Hoai Phuong| |
 | Interation: | Unscheduled 
 | Estimate: | 5 point


 ![Search_books](/uploads/513cb28bcf3ce782409ba7b1134a0aa6/Search_books.png)



